module aisco.automaticreport.twolevel {
    requires vmj.javalin.base;
    requires aisco.automaticreport.core;
    requires aisco.chartofaccount.core;
    requires aisco.financialreport.expense;
    requires aisco.financialreport.core;
    requires aisco.financialreport.income;

    requires static lombok;
    requires io.javalin;

    exports aisco.automaticreport.twolevel;
}