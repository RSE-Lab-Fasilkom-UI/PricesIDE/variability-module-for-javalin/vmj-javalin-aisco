package aisco.automaticreport.twolevel;

import aisco.automaticreport.core.controller.AutomaticReportController;
import aisco.automaticreport.core.decorator.AutomaticReportControllerDecorator;
import aisco.automaticreport.core.service.AutomaticReportService;
import aisco.chartofaccount.core.model.ChartOfAccount;
import aisco.chartofaccount.core.model.ChartOfAccountEntry;
import aisco.financialreport.core.model.FinancialReport;
import aisco.financialreport.income.Income;
import io.javalin.http.Handler;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.utils.function.DiFunctionCall;
import vmj.javalin.base.utils.function.MonoFunctionCall;
import vmj.javalin.base.utils.function.NoParamFunctionCall;
import vmj.javalin.base.utils.function.TriFunctionCall;
import vmj.javalin.base.web.component.MethodValue;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TwoLevelAutomaticReportController extends AutomaticReportControllerDecorator {

    private AutomaticReportService automaticReportService = new AutomaticReportService();

    public TwoLevelAutomaticReportController(AutomaticReportController automaticReportController) {
        super(automaticReportController);
        automaticReportController.updateRoute("list", twoLevelList);
    }

    private Consumer<ChartOfAccountEntry> generateRandomCoa = (entry) -> {
        String id = String.format("%s0000", Integer.toString(entry.getIdChartOfAccount()));
        id = id.substring(0, 5);
        ChartOfAccount chartOfAccount = new ChartOfAccount();
        chartOfAccount.setId(Integer.parseInt(id));
        chartOfAccount.setName("Coa Basic Name "+ id);
        chartOfAccount.setDescription(" Coa from "+id);

        DatabaseUtilsInMemory.createData("chartofaccount", id, chartOfAccount);
    };

    private Handler twoLevelList = ctx -> {
        List<Income> allIncomes = (List<Income>) DatabaseUtilsInMemory.getRecordsData("income").values().stream()
                .collect(Collectors.toCollection(ArrayList::new));
        List<FinancialReport> allExpenses = (List<FinancialReport>) DatabaseUtilsInMemory.getRecordsData("expense").values().stream()
                .collect(Collectors.toCollection(ArrayList::new));

        allIncomes.forEach(System.out::println);
        allExpenses.forEach(System.out::println);

        List<ChartOfAccountEntry> coaSheets = new ArrayList<>();


        TriFunctionCall<List<Income>, List<FinancialReport>, Integer, List<ChartOfAccountEntry>>
                transformToChartOfAccount = automaticReportService.getMethodByName("transformToChartOfAccount");

        List<ChartOfAccountEntry> coaLevel1 = transformToChartOfAccount.execute(
                allIncomes,
                allExpenses,
                1
        );

        coaLevel1.forEach(System.out::println);

        coaLevel1.forEach(entry ->generateRandomCoa.accept(entry) );


        List<ChartOfAccountEntry> coaLevel2 = transformToChartOfAccount.execute(
                allIncomes,
                allExpenses,
                2
        );

        coaLevel2.forEach(entry ->generateRandomCoa.accept(entry) );
        coaLevel2.forEach(System.out::println);


        DiFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>, List<ChartOfAccountEntry>>
                getOperationalActivity = automaticReportService.getMethodByName("getOperationalActivity");

        coaSheets = getOperationalActivity.execute(
                coaLevel1,
                coaLevel2
        );

        System.out.println("Get operational Activity");
        coaSheets.forEach(System.out::println);
        System.out.println("=====================");


        NoParamFunctionCall<List<ChartOfAccountEntry>> getInvestActivity = automaticReportService.getMethodByName("getInvestActivity");
        NoParamFunctionCall<List<ChartOfAccountEntry>> getFundingActivity = automaticReportService.getMethodByName("getFundingActivity");
        MonoFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>> getCashFlowSummary = automaticReportService.getMethodByName("getCashFlowSummary");
        DiFunctionCall<String, Integer, ChartOfAccountEntry> createChartOfAccountEntry = automaticReportService.getMethodByName("createChartOfAccountEntry");




        List<ChartOfAccountEntry> investActivity = getFundingActivity.execute();
        List<ChartOfAccountEntry> fundingActivity = getInvestActivity.execute();
        List<ChartOfAccountEntry> cashFlowSummary = getCashFlowSummary.execute(coaLevel1);


        ChartOfAccountEntry emptyRow = createChartOfAccountEntry.execute(
                "",
                0
        );

        coaSheets.addAll(investActivity);
        coaSheets.addAll(fundingActivity);
        coaSheets.add(emptyRow);
        coaSheets.addAll(cashFlowSummary);

        coaSheets.forEach(System.out::println);

        ctx.json(coaSheets);
    };

}
