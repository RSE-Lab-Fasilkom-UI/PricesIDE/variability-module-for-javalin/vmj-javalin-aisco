package aisco.summary.core.model;

import lombok.Data;

@Data
public class Summary {
    private int id;
    private String datestamp;
    private String description;
    private int income;
    private int expense;
    private String programName;
}
