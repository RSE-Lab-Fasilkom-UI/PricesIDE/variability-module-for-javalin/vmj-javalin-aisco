package aisco.program.activity;

import aisco.program.core.controller.ProgramController;
import aisco.program.core.decorator.ProgramControllerDecorator;
import aisco.program.core.model.Program;
import io.javalin.http.Handler;

import java.util.HashMap;

public class ActivityController extends ProgramControllerDecorator {

    public ActivityController(ProgramController programController) {
        super(programController);
        programController.setDomainName("activity");
        programController.addRoute("setIdActivity/{idProgram}", setIdActivity, "POST");

    }

    private Handler setIdActivity = ctx -> {
        int idProgram = Integer.parseInt(ctx.pathParam("idProgram"));
        HashMap<String, Integer> requestBody = ctx.bodyAsClass(HashMap.class);
        int idActivity = requestBody.get("idActivity");

        Program targetProgram = ProgramController.getDatabase().get(idProgram);
        ProgramActivity programActivity = new ProgramActivity(targetProgram);
        programActivity.setIdActivity(idActivity);

        ProgramController.getDatabase().put(idProgram, programActivity);
        ctx.json(programActivity);

    } ;

}
