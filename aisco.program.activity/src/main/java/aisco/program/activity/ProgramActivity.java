package aisco.program.activity;

import aisco.program.core.decorator.ProgramDecorator;
import aisco.program.core.model.Program;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProgramActivity extends ProgramDecorator {

    public ProgramActivity(Program program) {
        super(program);
    }

    private int idActivity;



}
