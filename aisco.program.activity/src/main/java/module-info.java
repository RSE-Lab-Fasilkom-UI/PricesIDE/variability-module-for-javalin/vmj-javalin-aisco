module aisco.program.activity {

    requires aisco.program.core;
    requires io.javalin;
    requires static lombok;
    requires vmj.javalin.base;

    exports aisco.program.activity;

}