package aisco.financialreport.expense;

import aisco.financialreport.core.controller.FinancialReportController;
import aisco.financialreport.core.decorator.FinancialReportControllerDecorator;
import aisco.financialreport.core.model.FinancialReport;
import io.javalin.http.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vmj.javalin.base.productline.annotations.Delta;
import vmj.javalin.base.productline.annotations.Scope;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Delta(name = "DExpense", scope = Scope.CONTROLLER, modifies = "FinancialReport")
public class ExpenseController extends FinancialReportControllerDecorator {

    private Logger log = LoggerFactory.getLogger(ExpenseController.class);

    public ExpenseController(FinancialReportController financialReportController) {
        super(financialReportController);
        financialReportController.setDomainName("expense");
        financialReportController.addRoute("save", createExpense, "POST");
        financialReportController.addRoute("update", updateExpense, "PUT");
        financialReportController.addRoute("list", findAllExpense, "GET");
        financialReportController.addRoute("printHeaderExpense", printHeaderExpense, "GET");
        financialReportController.addRoute("sumExpense", sumExpense, "GET");
    }

    private Handler printHeaderExpense = ctx -> {
        ctx.header("Financial Report - Delta - Expense");
    };

    private Handler createExpense = ctx -> {
        log.info("Incoming Request");
        FinancialReport expense = ctx.bodyAsClass(FinancialReport.class);
        DatabaseUtilsInMemory.createData("expense", String.valueOf(expense.getId()),expense);
        ctx.json(expense);
    };

    private Handler findAllExpense = ctx -> {
        Map expenses = DatabaseUtilsInMemory.getRecordsData("expense");
        HashMap<String, Object> response = new HashMap<>();
        response.put("data", expenses.values());
        ctx.json(response);
    };

    private Handler updateExpense = ctx -> {
      FinancialReport updatedExpense = ctx.bodyAsClass(FinancialReport.class);
      DatabaseUtilsInMemory.updateData("expense", String.valueOf(updatedExpense.getId()), updatedExpense);
      ctx.json(updatedExpense);
    };

    private Handler sumExpense = ctx -> {
        Map expenses = DatabaseUtilsInMemory.getRecordsData("expense");
        Map<String, Integer> response = new HashMap<>();
        response.put("totalExpense", 0);
        expenses.values().forEach(rawExpense -> {
            FinancialReport expense = (FinancialReport) rawExpense;
            response.put("totalExpense", response.get("totalExpense")+((FinancialReport) rawExpense).getAmount());
        });
        ctx.json(response);
    };
}
