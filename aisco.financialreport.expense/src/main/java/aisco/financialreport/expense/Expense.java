package aisco.financialreport.expense;

import aisco.financialreport.core.decorator.FinancialReportDecorator;
import aisco.financialreport.core.model.FinancialReport;
import vmj.javalin.base.productline.annotations.Delta;
import vmj.javalin.base.productline.annotations.Scope;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "financial_report_expense")
@Delta(name = "DExpense", scope = Scope.MODEL, modifies = "FinancialReport")
public class Expense extends FinancialReportDecorator {

    public Expense(FinancialReport financialReport) {
        super(financialReport);
    }
}
