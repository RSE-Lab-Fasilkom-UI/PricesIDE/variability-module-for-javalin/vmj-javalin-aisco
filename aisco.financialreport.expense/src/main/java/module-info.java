module aisco.financialreport.expense {
    requires io.javalin;
    requires vmj.javalin.base;
    requires aisco.financialreport.core;
    requires org.slf4j;
    requires java.persistence;
    requires org.hibernate.orm.core;

    opens aisco.financialreport.expense;

    exports aisco.financialreport.expense;
}