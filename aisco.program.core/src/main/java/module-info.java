module aisco.program.core {
    requires io.javalin;
    requires transitive static lombok;
    requires transitive com.fasterxml.jackson.core;
    requires vmj.javalin.base;
    requires org.slf4j;
    exports aisco.program.core.controller;
    exports aisco.program.core.model;
    exports aisco.program.core.decorator;
}