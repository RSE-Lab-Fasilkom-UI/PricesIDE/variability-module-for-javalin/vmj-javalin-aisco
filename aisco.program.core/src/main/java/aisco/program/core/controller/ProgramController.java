package aisco.program.core.controller;

import aisco.program.core.model.Program;
import io.javalin.http.Handler;
import lombok.Getter;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.web.Controller;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Getter
public class ProgramController extends Controller {

    private static int currentId = 0;
    private static Map<Integer, Program> inMemoryDatabase = new HashMap<>();

    public ProgramController() {
        setDomainName("program");
        addRoute("list", findAllProgram, "GET");
        addRoute("save", createProgram, "POST");
        addRoute("setExecutionDate", setExecutionDate, "POST");
    }

    private Handler createProgram = ctx -> {
        Program program = ctx.bodyAsClass(Program.class);
        program.setIdProgram(currentId++);
        inMemoryDatabase.put(program.getIdProgram(), program);
        DatabaseUtilsInMemory.createData("program", String.valueOf(program.getIdProgram()),program);
        ctx.json(program);
    };

    private Handler findAllProgram = ctx -> {
        Map allProgram = DatabaseUtilsInMemory.getRecordsData("program");
        ctx.json(allProgram.values());
    };

    private Handler setExecutionDate = ctx -> {
        int idProgram = Integer.parseInt(Objects.requireNonNull(ctx.queryParam("idProgram")));
        String newExecutionDate = ctx.body();

        Program targetProgram = inMemoryDatabase.get(idProgram);
        targetProgram.setExecutionDate(newExecutionDate);
        ctx.json(targetProgram);

    };

    protected static Map<Integer, Program> getDatabase() {
        return inMemoryDatabase;
    }



}
