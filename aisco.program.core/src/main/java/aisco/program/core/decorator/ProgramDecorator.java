package aisco.program.core.decorator;

import aisco.program.core.model.Program;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class ProgramDecorator extends Program {

    private Program program;

    public ProgramDecorator() {
    }

    public ProgramDecorator(Program program) {
        this.program = program;
    }

    @Override
    public int getIdProgram() {
        return getProgram().getIdProgram();
    }

    @Override
    public String getName() {
        return getProgram().getName();
    }

    @Override
    public String getTarget() {
        return getProgram().getTarget();
    }

    @Override
    public String getPartner() {
        return getProgram().getPartner();
    }

    @Override
    public String getLogoUrl() {
        return getProgram().getLogoUrl();
    }

    @Override
    public String getExecutionDate() {
        return getProgram().getExecutionDate();
    }

    @Override
    public void setIdProgram(int idProgram) {
        getProgram().setIdProgram(idProgram);
    }

    @Override
    public void setName(String name) {
        getProgram().setName(name);
    }

    @Override
    public void setTarget(String target) {
        getProgram().setTarget(target);
    }

    @Override
    public void setPartner(String partner) {
        getProgram().setPartner(partner);
    }

    @Override
    public void setLogoUrl(String logoUrl) {
        getProgram().setLogoUrl(logoUrl);
    }

    @Override
    public void setExecutionDate(String executionDate) {
        getProgram().setExecutionDate(executionDate);
    }
}
