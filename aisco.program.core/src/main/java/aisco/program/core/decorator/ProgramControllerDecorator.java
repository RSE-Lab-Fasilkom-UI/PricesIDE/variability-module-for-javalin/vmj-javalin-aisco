package aisco.program.core.decorator;

import aisco.program.core.controller.ProgramController;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public abstract class ProgramControllerDecorator extends ProgramController {

    private ProgramController programController;



}
