package aisco.program.core.factory;

import aisco.program.core.model.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

public class ProgramFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProgramFactory.class);

    public static Program createProgram(String fullyQualifiedName, Object... base) {
        Program record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            record = (Program) constructor.newInstance(base);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Failed to create instance of Program.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Failed to run: Check your constructor argument");
            System.exit(20);
        } catch (ClassCastException e) {
            LOGGER.error("Failed to create instance of Program.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Failed to cast the object");
            System.exit(30);
        } catch (ClassNotFoundException e) {
            LOGGER.error("Failed to create instance of Program.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Decorator can't be applied to the object");
            System.exit(40);
        } catch (Exception e) {
            LOGGER.error("Failed to create instance of Program.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            System.exit(50);
        }
        return record;
    }

}
