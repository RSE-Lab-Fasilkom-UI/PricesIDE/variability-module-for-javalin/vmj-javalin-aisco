package aisco.program.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Program {

    private int idProgram;
    private String name;
    private String target;
    private String partner;
    private String logoUrl;
    private String executionDate;
}
