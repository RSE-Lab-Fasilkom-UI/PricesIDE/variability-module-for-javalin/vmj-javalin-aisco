module aisco.automaticreport.core {

    requires vmj.javalin.base;
    requires aisco.financialreport.income;
    requires aisco.financialreport.core;
    requires aisco.chartofaccount.core;
    requires io.javalin;
    requires static lombok;

    exports aisco.automaticreport.core.controller;
    exports aisco.automaticreport.core.decorator;
    exports aisco.automaticreport.core.service;
    
}