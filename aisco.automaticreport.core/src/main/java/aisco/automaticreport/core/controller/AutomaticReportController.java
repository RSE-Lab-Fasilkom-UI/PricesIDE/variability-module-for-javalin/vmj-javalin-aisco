package aisco.automaticreport.core.controller;

import aisco.automaticreport.core.service.AutomaticReportService;
import aisco.chartofaccount.core.model.ChartOfAccount;
import aisco.chartofaccount.core.model.ChartOfAccountEntry;
import aisco.financialreport.core.model.FinancialReport;
import aisco.financialreport.income.Income;
import io.javalin.http.Handler;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.utils.function.MonoFunctionCall;
import vmj.javalin.base.utils.function.NoParamFunctionCall;
import vmj.javalin.base.utils.function.TriFunctionCall;
import vmj.javalin.base.web.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AutomaticReportController extends Controller {

    private final AutomaticReportService automaticReportService = new AutomaticReportService();

    public AutomaticReportController() {
        setDomainName("automatic-report");
        addRoute("getResponse", getBasicResponse, "GET");
        addRoute("list", list, "GET");
    }


    private Consumer<ChartOfAccountEntry> generateRandomCoa = (entry) -> {
        String id = String.format("%s0000", Integer.toString(entry.getIdChartOfAccount()));
        id = id.substring(0, 5);
        ChartOfAccount chartOfAccount = new ChartOfAccount();
        chartOfAccount.setId(Integer.parseInt(id));
        chartOfAccount.setName("Coa Basic Name "+ id);
        chartOfAccount.setDescription(" Coa from "+id);

        DatabaseUtilsInMemory.createData("chartofaccount", id, chartOfAccount);
    };

    private Handler getBasicResponse = ctx -> {

        NoParamFunctionCall<String> createResponse = automaticReportService.getMethodByName("create-response");
        String response = createResponse.execute();
        ctx.json(response);
    };

    private Handler list = ctx -> {
        List<Income> allIncomes = (List<Income>) DatabaseUtilsInMemory.getRecordsData("income").values().stream()
                .collect(Collectors.toCollection(ArrayList::new));
        List<FinancialReport> allExpenses = (List<FinancialReport>) DatabaseUtilsInMemory.getRecordsData("expense").values().stream()
                .collect(Collectors.toCollection(ArrayList::new));

        allIncomes.forEach(System.out::println);
        allExpenses.forEach(System.out::println);



        TriFunctionCall<List<Income>, List<FinancialReport>, Integer,
                List<ChartOfAccountEntry>> transformToChartOfAccount = automaticReportService
                .getMethodByName("transformToChartOfAccount");

        List<ChartOfAccountEntry> coaLevel1 = transformToChartOfAccount.execute(allIncomes, allExpenses, 1);

        coaLevel1.forEach(System.out::println);

        coaLevel1.forEach(entry ->generateRandomCoa.accept(entry) );


        MonoFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>> addDetail = automaticReportService
                .getMethodByName("addDetail");

        coaLevel1 = addDetail.execute(coaLevel1);

        ctx.json(coaLevel1);

    };


}
