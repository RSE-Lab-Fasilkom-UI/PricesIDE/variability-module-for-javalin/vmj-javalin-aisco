package aisco.automaticreport.core.decorator;


import aisco.automaticreport.core.controller.AutomaticReportController;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AutomaticReportControllerDecorator extends AutomaticReportController {
    private AutomaticReportController automaticReportController;

}
