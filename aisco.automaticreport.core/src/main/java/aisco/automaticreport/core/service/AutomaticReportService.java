package aisco.automaticreport.core.service;

import aisco.chartofaccount.core.model.ChartOfAccount;
import aisco.chartofaccount.core.model.ChartOfAccountEntry;
import aisco.financialreport.core.model.FinancialReport;
import aisco.financialreport.income.Income;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.utils.function.DiFunctionCall;
import vmj.javalin.base.utils.function.MonoFunctionCall;
import vmj.javalin.base.utils.function.NoParamFunctionCall;
import vmj.javalin.base.utils.function.TriFunctionCall;
import vmj.javalin.base.web.Service;

import java.util.*;
import java.util.stream.Collectors;

public class AutomaticReportService extends Service {

    public AutomaticReportService() {
        addService("create-response", createAutomaticStupidResponse);
        addService("transformToChartOfAccount", transformToChartOfAccount);
        addService("addDetail", addDetail);
        addService("createChartOfAccountEntry", createChartOfAccountEntry);
        addService("removeEntry", removeEntry);
        addService("updateCoas", updateCoas);
        addService("getAmount", getAmount);
        addService("getOperationalActivity", getOperationalActivity);
        addService("getInvestActivity", getInvestActivity);
        addService("getFundingActivity", getFundingActivity);
        addService("getCashFlowSummary", getCashFlowSummary);

    }

    private NoParamFunctionCall<String> createAutomaticStupidResponse = () -> {
        return "Basic Response Get!";
    };

    private final TriFunctionCall<FinancialReport, Integer, HashMap<String, ChartOfAccountEntry>, Optional<Void>>
            transformAFinancialReport = (income, codeLength, entries) -> {
        String code = income.getIdCoa().substring(0, codeLength);
        ChartOfAccountEntry coaFromDatabase = entries.get(code);
        if (coaFromDatabase != null) {
            coaFromDatabase.setAmount(coaFromDatabase.getAmount() + income.getAmount());
            entries.put(code, coaFromDatabase);
        } else {
            ChartOfAccountEntry newEntry = new ChartOfAccountEntry();
            newEntry.setAmount(income.getAmount());
            newEntry.setIdChartOfAccount(Integer.parseInt(code));
            newEntry.setLevel(codeLength);
            entries.put(code, newEntry);
        }
        return Optional.empty();
    };

    private final TriFunctionCall<List<Income>, List<FinancialReport>, Integer, List<ChartOfAccountEntry>>
            transformToChartOfAccount = (incomes, expenses, codeLength) -> {
        HashMap<String, ChartOfAccountEntry> entries = new HashMap<>();

        for (Income income : incomes) {
            transformAFinancialReport.execute(income, codeLength, entries);
        }

        for (FinancialReport expense : expenses) {
            transformAFinancialReport.execute(expense, codeLength, entries);
        }
        List<ChartOfAccountEntry> entryList = entries.values().stream()
                .sorted(Comparator.comparing(ChartOfAccountEntry::getIdChartOfAccount))
                .collect(Collectors.toCollection(ArrayList::new));
        return entryList;
    };

    private MonoFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>> addDetail = (coaSheets) -> {
        System.out.println("Printing params");
        System.out.println(coaSheets);
        for (ChartOfAccountEntry entry : coaSheets) {
            String id = String.format("%s0000000", Integer.toString(entry.getIdChartOfAccount()));
            id = id.substring(0, 5);

            System.out.println(id);

            ChartOfAccount chartOfAccount = (ChartOfAccount) DatabaseUtilsInMemory.getRowData("chartofaccount", id);
            try {
                entry.setDescription(chartOfAccount.getDescription());
                entry.setName(chartOfAccount.getName());
                entry.setIdChartOfAccount(Integer.parseInt(id));
            } catch (NullPointerException e) {
                throw new RuntimeException(e.getMessage());
            }
            System.out.println("Printing Entry");
            System.out.println(entry);
        }
        return coaSheets;
    };

    private DiFunctionCall<String, Integer, ChartOfAccountEntry> createChartOfAccountEntry = (coaName, idCoa) -> {
        ChartOfAccountEntry chartOfAccount = new ChartOfAccountEntry();
        chartOfAccount.setName(coaName);
        chartOfAccount.setIdChartOfAccount(idCoa);
        return chartOfAccount;
    };

    private DiFunctionCall<List<ChartOfAccountEntry>, Set<String>, List<ChartOfAccountEntry>> removeEntry =
            (entries, headIdCoas) -> {
                List<ChartOfAccountEntry> result = new ArrayList<>();
                for (ChartOfAccountEntry entry : entries) {
                    String headIdCoa = Integer.toString(entry.getIdChartOfAccount()).substring(0, 1);
                    boolean isRestricted = headIdCoas.contains(headIdCoa);
                    if (!isRestricted) {
                        result.add(entry);
                    }
                }
                System.out.println("Checking result");
                result.forEach(System.out::println);
                return result;
            };

    private MonoFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>> updateCoas = (entries) -> {
        List<ChartOfAccountEntry> result = new ArrayList<>();
        for (ChartOfAccountEntry entry : entries) {
            String newName = String.format("Total %s", entry.getName());
            String newStrIdCoa = String.format("%s99", entry.getIdChartOfAccount());

            entry.setLevel(2);
            entry.setName(newName);
            entry.setIdChartOfAccount(Integer.parseInt(newStrIdCoa));

            result.add(entry);
        }
        return result;
    };

    private MonoFunctionCall<List<ChartOfAccountEntry>, Integer> getAmount = (coaLevel1) -> {
        int amount = 0;
        for (ChartOfAccountEntry entry : coaLevel1) {
            String headIdCoa = Integer.toString(entry.getIdChartOfAccount()).substring(0, 1);
            int coaAmount = entry.getAmount();
            if (headIdCoa.equalsIgnoreCase("6")) {
                amount -= coaAmount;
            } else {
                amount += coaAmount;
            }
        }
        return amount;
    };

    private DiFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>, List<ChartOfAccountEntry>>
            getOperationalActivity = (coaLevel1, coaLevel2) -> {
        ChartOfAccountEntry header = createChartOfAccountEntry.execute("Aktivitas Operasional", 91000);
        List<ChartOfAccountEntry> coaSheets = new ArrayList<>();
        coaSheets.addAll(coaLevel1);
        coaSheets.addAll(coaLevel2);
        System.out.println("Coa level 1 and 2");
        coaSheets.forEach(System.out::println);
        System.out.println("=====================");
        Set<String> restrictedHeadId = new HashSet<String>();
        restrictedHeadId.add("1");
        restrictedHeadId.add("2");
        coaSheets = removeEntry.execute(coaSheets, restrictedHeadId);
        coaSheets = addDetail.execute(coaSheets);


        List<ChartOfAccountEntry> coaLevel1Updated = removeEntry.execute(
                coaLevel1, restrictedHeadId
        );
        coaLevel1Updated = addDetail.execute(
                coaLevel1Updated
        );


        coaLevel1Updated = updateCoas.execute(
                coaLevel1Updated
        );

        coaSheets.addAll(coaLevel1Updated);

        coaSheets.forEach(System.out::println);

        int netAmount = getAmount.execute(coaLevel1Updated);


        ChartOfAccountEntry netEntry = createChartOfAccountEntry.execute(
                "Kas Bersih yang diterima (digunakan) untuk Aktivitas Operasi",
                91000
        );


        netEntry.setAmount(netAmount);

        coaSheets.add(netEntry);
        coaSheets.add(0, header);
        return coaSheets;
    };

    private NoParamFunctionCall<List<ChartOfAccountEntry>> getInvestActivity = () -> {
        List<ChartOfAccountEntry> result = new ArrayList<>();

        ChartOfAccountEntry header = createChartOfAccountEntry.execute(
                "Aktivitas Investasi",
                92000
        );

        ChartOfAccountEntry netEntry = createChartOfAccountEntry.execute(
                "Kas Bersih yang diterima (digunakan) untuk Aktivitas Investasi",
                92000
        );

        result.add(header);
        result.add(netEntry);

        return result;
    };


    private NoParamFunctionCall<List<ChartOfAccountEntry>> getFundingActivity = () -> {
        List<ChartOfAccountEntry> result = new ArrayList<>();

        ChartOfAccountEntry header = createChartOfAccountEntry.execute(
                "Aktivitas Pendanaan",
                93000
        );


        ChartOfAccountEntry netEntry = createChartOfAccountEntry.execute(
                "Kas Bersih yang diterima (digunakan) untuk Aktivitas Pendanaan",
                93000
        );

        result.add(header);
        result.add(netEntry);

        return result;
    };


    private MonoFunctionCall<List<ChartOfAccountEntry>, List<ChartOfAccountEntry>> getCashFlowSummary = (coaLevel1) -> {
        ChartOfAccountEntry netto = createChartOfAccountEntry.execute(
                "Kenaikan (Penurunan) netto dalam kas dan setara kas",
                94000
        );

        Set<String> notStartYearHead = new HashSet<String>();
        notStartYearHead.add("3");
        notStartYearHead.add("4");
        notStartYearHead.add("5");
        notStartYearHead.add("6");


        List<ChartOfAccountEntry> beginEntries = removeEntry.execute(
                coaLevel1,
                notStartYearHead
        );


        int beginAmount = getAmount.execute(
                beginEntries
        );


        ChartOfAccountEntry beginningEntry = createChartOfAccountEntry.execute(
                "Kas pada awal tahun",
                95000
        );

        beginningEntry.setAmount(beginAmount);

        Set<String> notEndYearHead = new HashSet<String>();
        notEndYearHead.add("1");
        notEndYearHead.add("2");

        List<ChartOfAccountEntry> netEntries = removeEntry.execute(
                coaLevel1,
                notEndYearHead
        );


        int netAmount = getAmount.execute(netEntries);

        int endingAmount = netAmount + beginAmount;

        ChartOfAccountEntry endingEntry = createChartOfAccountEntry.execute(
                "Kas pada akhir tahun",
                96000
        );

        endingEntry.setAmount(endingAmount);

        List<ChartOfAccountEntry> result = new ArrayList<>();
        result.add(netto);
        result.add(beginningEntry);
        result.add(endingEntry);

        return result;
    };


}
