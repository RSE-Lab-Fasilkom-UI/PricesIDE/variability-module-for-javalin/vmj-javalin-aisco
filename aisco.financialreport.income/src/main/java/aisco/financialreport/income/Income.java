package aisco.financialreport.income;

import aisco.financialreport.core.decorator.FinancialReportDecorator;
import aisco.financialreport.core.model.FinancialReport;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vmj.javalin.base.productline.annotations.Delta;
import vmj.javalin.base.productline.annotations.Scope;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter  @Setter
@Entity
@Table(name = "financial_report_income")
@Delta(name = "DIncome", scope = Scope.MODEL, modifies = "FinancialReport")
public class Income extends FinancialReportDecorator  {

    public Income(FinancialReport financialReport) {
        super(financialReport);
    }

    private String paymentMethod;

}
