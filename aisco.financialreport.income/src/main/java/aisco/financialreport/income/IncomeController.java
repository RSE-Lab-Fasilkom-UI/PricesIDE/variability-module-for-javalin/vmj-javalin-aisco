package aisco.financialreport.income;

import aisco.financialreport.core.controller.FinancialReportController;
import aisco.financialreport.core.decorator.FinancialReportControllerDecorator;
import aisco.financialreport.core.model.FinancialReport;
import io.javalin.http.Handler;
import vmj.javalin.base.productline.annotations.Delta;
import vmj.javalin.base.productline.annotations.Scope;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Delta(name = "DIncome", scope = Scope.CONTROLLER, modifies = "FinancialReport")
public class IncomeController extends FinancialReportControllerDecorator {

    public IncomeController(FinancialReportController financialReportController) {
        super(financialReportController);
        financialReportController.setDomainName("income");
        financialReportController.addRoute("create", createIncome, "POST");
        financialReportController.addRoute("update", updateIncome, "PUT");
        financialReportController.addRoute("list", findAllIncome, "GET");
        financialReportController.addRoute("sumIncome", sumIncome, "GET");
    }


    private Function<IncomeDTO, FinancialReport> setFinancialReportIncomeBaseData =  (incomeDTO) -> {
        FinancialReport financialReport = new FinancialReport();
        financialReport.setAmount(incomeDTO.getAmount());
        financialReport.setDatestamp(incomeDTO.getDatestamp());
        financialReport.setDescription(incomeDTO.getDescription());
        financialReport.setId(incomeDTO.getId());
        financialReport.setIdCoa(incomeDTO.getIdCoa());
        financialReport.setIdProgram(String.valueOf(incomeDTO.getIdProgram()));
        return financialReport;
    };

    private Handler createIncome = ctx -> {
        IncomeDTO incomeDTO = ctx.bodyAsClass(IncomeDTO.class);
        FinancialReport financialReport = setFinancialReportIncomeBaseData.apply(incomeDTO);
        Income income = new Income(financialReport);
        income.setPaymentMethod(incomeDTO.getPaymentMethod());
        DatabaseUtilsInMemory.createData("income", String.valueOf(income.getId()), income);
        ctx.json(income);
    };



    private Handler findAllIncome = ctx -> {
      Map incomes = DatabaseUtilsInMemory.getRecordsData("income");
      ctx.json(incomes.values());
    };

    private Handler updateIncome = ctx -> {
      IncomeDTO updatedIncomeDTO = ctx.bodyAsClass(IncomeDTO.class);

      String id = ctx.pathParam("idIncome");
    };

    private Handler sumIncome = ctx -> {
        Map incomes = DatabaseUtilsInMemory.getRecordsData("income");
        final Map<String, Integer>  response = new HashMap<>();
        response.put("totalIncome", 0);
        incomes.values().forEach(rawIncome -> {
            Income convertedIncome = (Income) rawIncome;
            response.put("totalIncome", response.get("totalIncome")+ convertedIncome.getAmount());
        });
        ctx.json(response);
    };




}
