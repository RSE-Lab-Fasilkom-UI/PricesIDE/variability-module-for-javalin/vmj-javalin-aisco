package aisco.financialreport.income;

import lombok.Data;

@Data
public class IncomeDTO {
    private int id;
    private String datestamp;
    private int amount;
    private String description;
    private int idProgram;
    private String idCoa;
    private String paymentMethod;
}
