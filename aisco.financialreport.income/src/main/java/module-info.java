module aisco.financialreport.income {

    requires io.javalin;
    requires static lombok;
    requires vmj.javalin.base;
    requires aisco.financialreport.core;
    requires java.persistence;
    requires org.hibernate.orm.core;

    opens aisco.financialreport.income;

    exports aisco.financialreport.income;
}