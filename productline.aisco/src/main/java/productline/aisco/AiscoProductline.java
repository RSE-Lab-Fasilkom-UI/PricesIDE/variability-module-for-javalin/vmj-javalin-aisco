package productline.aisco;

import vmj.javalin.base.productline.ProductLine;
import vmj.javalin.base.productline.configurator.CoreConfigurator;
import vmj.javalin.base.productline.configurator.DeltaConfigurator;

public class AiscoProductline extends ProductLine {

    public AiscoProductline() {
        super("Aisco");
    }

    @Override
    public void configureProductLine() {

        CoreConfigurator coreConfigurator = new CoreConfigurator();
        DeltaConfigurator deltaConfigurator = new DeltaConfigurator();


        coreConfigurator.define("FinancialReport").when("FinancialReport").save();
        deltaConfigurator.define("DExpense").when("Expense").modifies("FinancialReport").save();
        deltaConfigurator.define("DIncome").when("Income").modifies("FinancialReport").save();

    }
}
