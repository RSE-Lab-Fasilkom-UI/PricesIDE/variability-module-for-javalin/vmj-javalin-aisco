module productline.aisco {
    requires vmj.javalin.base;

    opens productline.aisco;

    exports productline.aisco;
}