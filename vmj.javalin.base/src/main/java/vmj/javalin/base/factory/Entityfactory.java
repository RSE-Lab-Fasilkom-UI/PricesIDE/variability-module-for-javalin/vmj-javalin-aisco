package vmj.javalin.base.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

public class Entityfactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(Entityfactory.class);

    public static <E> E createEntity(String fullyQualifiedName, Object... base) {
        Class<?> clz = null;
        try {
            clz = Class.forName(fullyQualifiedName);
        } catch (ClassNotFoundException e) {
            LOGGER.error("Failed to create instance.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Decorator can't be applied to the object");
            System.exit(40);
        } catch (Exception e) {
            LOGGER.error("Failed to create instance.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            System.exit(50);
        }
        return createEntity(clz, base);
    }

    public static <E> E createEntity(Class<?> clz, Object... base) {
        String fullyQualifiedName = clz.getName();
        E record = null;
        try {
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            record = (E) constructor.newInstance(base);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Failed to create instance.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Failed to run: Check your constructor argument");
            System.exit(20);
        } catch (ClassCastException e) {
            LOGGER.error("Failed to create instance");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error("Failed to cast the object");
            System.exit(30);
        } catch (Exception e) {
            LOGGER.error("Failed to create instance.");
            LOGGER.error("Given FQN: " + fullyQualifiedName);
            LOGGER.error(e.getCause().toString());
            System.exit(50);
        }
        return record;
    }


}
