package vmj.javalin.base.data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vmj.javalin.base.utils.container.Pair;
import vmj.javalin.base.utils.function.DiFunctionCall;
import vmj.javalin.base.utils.function.TriFunctionCall;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Repository<E> {
    private SessionFactory sessionFactory;
    private Logger logger = LoggerFactory.getLogger(Repository.class);
    private final Class<E> entityType;

    private DiFunctionCall<TriFunctionCall<Serializable, E, Session, List<E>>, Pair<Serializable, E>, List<E>> transactionTemplate =
            (function, parameters) -> {
        List<E> result = new ArrayList<>();
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

           result = function.execute(
                    parameters.getFirst(),
                    parameters.getSecond(),
                   session
            );
            transaction.commit();


        } catch (HibernateException e) {
            logger.info("There is an exception when initializing session");
        } catch (Exception e) {
            logger.info("Transaction failed... Rollbacking");
            rollbackIfTransactionExist(transaction);
        }
        return result;
    };


    private void rollbackIfTransactionExist(Transaction transaction) {
        if (transaction!=null) {
            transaction.rollback();
        }
    }




    public Repository(SessionFactory sessionFactory, Class<E> entityType) {
        this.sessionFactory = sessionFactory;
        this.entityType = entityType;
    }

    public E save(E entity) {

        TriFunctionCall<Serializable, E, Session, List<E>> saveOperation = (id, targetEntity, session ) -> {
            List<E> returnValues = new ArrayList<>();
            E savedEntity = (E) session.save(targetEntity);
            returnValues.add(savedEntity);
            return returnValues;
        };

        Pair<Serializable, E> parameters = new Pair<>();
        parameters.setFirst(0);
        parameters.setSecond(entity);

        List<E> result = transactionTemplate.execute(
                saveOperation,
                parameters
        );
        return result.get(0);
    }

    public E update(E entity) {
        TriFunctionCall<Serializable, E, Session, List<E>> updateOperation = (id, targetEntity, session ) -> {
            List<E> returnValues = new ArrayList<>();
            session.update(targetEntity);
            returnValues.add(targetEntity);
            return returnValues;
        };

        Pair<Serializable, E> parameters = new Pair<>();
        parameters.setFirst(0);
        parameters.setSecond(entity);

        List<E> result = transactionTemplate.execute(
                updateOperation,
                parameters
        );
        return result.get(0);
    }

    public E findById(Serializable id) {
        TriFunctionCall<Serializable, E, Session, List<E>> getEntityById = (entityId, entity, session) -> {
            List<E> returnValues = new ArrayList<>();
            entity = (E)session.get(entityType, entityId);
            returnValues.add(entity);
            return returnValues;
        };
        Pair<Serializable, E> parameters = new Pair<>();
        parameters.setFirst(id);
        parameters.setSecond(null);

        List<E> result = transactionTemplate.execute(
                getEntityById,
                parameters
        );
        return result.get(0);
    }

    public List<E> findAll() {
        TriFunctionCall<Serializable, E, Session, List<E>> findAll = (entityId, entity, session) -> {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<E> cq = cb.createQuery(entityType);
            Root<E> rootEntry = cq.from(entityType);
            CriteriaQuery<E> all = cq.select(rootEntry);

            TypedQuery<E> allQuery = session.createQuery(all);
            return allQuery.getResultList();
        };

        Pair<Serializable, E> parameters = new Pair<>();
        parameters.setFirst(0);
        parameters.setSecond(null);

        List<E> result = transactionTemplate.execute(
                findAll,
                parameters
        );
        return result;
    }
}
