package vmj.javalin.base.data;

public class UninitializedSessionException extends RuntimeException {


    public UninitializedSessionException(String message) {
        super("Uninitialized Session Factory: " + message);
    }
}
