package vmj.javalin.base.data;

import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class DatabaseConfigurationBuilder {
    private Configuration configuration = new Configuration();


    public DatabaseConfigurationBuilder addClass(Class annotatedClass) {
       configuration.addAnnotatedClass(annotatedClass);
        return this;
    }

    public Configuration build() {
        return configuration;
    }
}
