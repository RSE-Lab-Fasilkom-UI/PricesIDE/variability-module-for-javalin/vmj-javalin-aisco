package vmj.javalin.base.data;

import org.hibernate.SessionFactory;

public class HibernateUtils {
    private static SessionFactory sessionFactory;


    public HibernateUtils(DatabaseConfigurationBuilder databaseConfigurationBuilder) {
        buildSessionFactory(databaseConfigurationBuilder);
    }

    private synchronized void buildSessionFactory(DatabaseConfigurationBuilder databaseConfigurationBuilder) {
        if(sessionFactory==null) {
            createSessionFactory(databaseConfigurationBuilder);
        }

    }

    private void createSessionFactory(DatabaseConfigurationBuilder databaseConfigurationBuilder) {
        try {
            sessionFactory =  databaseConfigurationBuilder.build()
                    .buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("build SessionFactory failed :" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory==null) {
            throw new UninitializedSessionException("The constructor has not been initialized yet");
        }
        return sessionFactory;
    }
}
