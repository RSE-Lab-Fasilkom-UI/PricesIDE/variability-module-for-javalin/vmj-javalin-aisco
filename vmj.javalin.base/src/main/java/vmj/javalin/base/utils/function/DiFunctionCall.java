package vmj.javalin.base.utils.function;

@FunctionalInterface
public interface DiFunctionCall<P,Q,RV> extends FunctionCall{
    public RV execute(P param1,Q param2);
}
