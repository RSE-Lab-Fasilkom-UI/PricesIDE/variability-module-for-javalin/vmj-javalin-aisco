package vmj.javalin.base.utils.container;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Pair<F,S> {

    private F first;
    private S second;
}
