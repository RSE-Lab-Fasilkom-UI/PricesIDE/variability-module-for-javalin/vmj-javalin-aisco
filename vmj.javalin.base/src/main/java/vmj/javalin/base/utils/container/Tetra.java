package vmj.javalin.base.utils.container;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Tetra<A,B,C,D> {

    private A first;
    private B second;
    private C third;
    private D fourth;
}
