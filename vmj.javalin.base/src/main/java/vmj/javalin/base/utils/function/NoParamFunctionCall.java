package vmj.javalin.base.utils.function;

@FunctionalInterface
public interface NoParamFunctionCall<R> extends FunctionCall {
    public R execute();
}
