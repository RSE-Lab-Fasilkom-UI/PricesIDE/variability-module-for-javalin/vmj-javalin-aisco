package vmj.javalin.base.utils.function;

@FunctionalInterface
public interface TriFunctionCall<P,Q,R,RV> extends FunctionCall {
    public RV execute(P param1, Q param2, R param3);
}
