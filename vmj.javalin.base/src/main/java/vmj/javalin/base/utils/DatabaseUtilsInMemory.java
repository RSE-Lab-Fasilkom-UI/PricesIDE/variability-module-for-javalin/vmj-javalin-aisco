package vmj.javalin.base.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class DatabaseUtilsInMemory{

    private static Logger logger = LoggerFactory.getLogger(DatabaseUtilsInMemory.class);

    private static Map<String, Map> dataHolder = new HashMap<>();

    public static Object createData(String tableName, String primaryKey, Object attributesData) {
        logger.info("Create new Data");
        Map recordsData = getRecordsData(tableName);
        recordsData.put(primaryKey, attributesData);
        return attributesData;
    }


    public static Map getRecordsData(String tableName) {
        logger.info("Getting table "+ tableName);
        Map recordsData = dataHolder.get(tableName);
        if (recordsData == null) {
            logger.info("No record available, creating new one");
            dataHolder.put(tableName, new HashMap<String, Object>());
            logger.info("Record holder created");
            recordsData = dataHolder.get(tableName);
        }
        logger.debug(recordsData.toString());
        return recordsData;
    }

    public static Object getRowData(String tableName, String primaryKey) {
        Map recordsData = getRecordsData(tableName);
        return recordsData.get(primaryKey);
    }


    public static Object updateData(String tableName, String primaryKey, Object attributesData) throws RuntimeException {
        Map record  = getRecordsData(tableName);
        record.put(primaryKey, attributesData);
        return record;
    }


}
