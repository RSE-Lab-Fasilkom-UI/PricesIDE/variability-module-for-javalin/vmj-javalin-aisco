package vmj.javalin.base.utils.function;

@FunctionalInterface
public interface TetraFunctionCall<A, B, C, D, RV> extends FunctionCall {
    public RV execute(A param1,B param2, C param3, D param4);
}
