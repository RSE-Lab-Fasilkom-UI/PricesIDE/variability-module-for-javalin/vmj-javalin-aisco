package vmj.javalin.base.utils;

import java.util.HashMap;
import java.util.Map;

public interface DatabaseUtils<I> {

    public I createData(String tableName, String primaryKey, I attributesData);

    public I updateData(String tableName, I attributesData, String primaryKey)  ;

    public I deleteData(String tableName, String primaryKey) ;
}
