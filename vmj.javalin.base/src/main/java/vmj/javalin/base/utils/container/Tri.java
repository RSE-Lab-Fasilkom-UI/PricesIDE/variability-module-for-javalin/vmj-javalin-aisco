package vmj.javalin.base.utils.container;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Builder
public class Tri<F,S,T> {
    private F first;
    private S second;
    private T third;

}



