package vmj.javalin.base.utils.function;

@FunctionalInterface
public interface MonoFunctionCall<P,RV> extends FunctionCall {
    public RV execute(P param);
}
