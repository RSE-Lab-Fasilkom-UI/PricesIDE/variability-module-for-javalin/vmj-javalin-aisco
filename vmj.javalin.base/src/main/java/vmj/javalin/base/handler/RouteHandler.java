package vmj.javalin.base.handler;

import io.javalin.http.Handler;
import lombok.Data;

@Data
public class RouteHandler {

    private String httpMethod;
    private Handler handler;
}
