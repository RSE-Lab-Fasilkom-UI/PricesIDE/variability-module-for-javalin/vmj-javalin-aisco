package vmj.javalin.base.web;

import io.javalin.Javalin;
import io.javalin.http.Handler;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vmj.javalin.base.handler.RouteHandler;

import java.util.*;
import java.util.stream.Collectors;

@Data
public abstract class Controller {
    private Javalin app;
    private Map<String, RouteHandler> routeHandler = new HashMap<>();
    private String domainName;
    private Logger logger = LoggerFactory.getLogger(Controller.class);



    public void registerRoute(Javalin app) {
        this.app = app;
        initRouteHandler(app);
    }

    protected void initRouteHandler(Javalin app) {
        for(String route: routeHandler.keySet()) {
            RouteHandler handler = routeHandler.get(route);
            if (handler.getHttpMethod().equalsIgnoreCase("get")) {
                app.get(route, handler.getHandler());
            } else if(handler.getHttpMethod().equalsIgnoreCase("post")) {
                app.post(route, handler.getHandler());
            } else if(handler.getHttpMethod().equalsIgnoreCase("delete")) {
                app.delete(route, handler.getHandler());
            } else if (handler.getHttpMethod().equalsIgnoreCase("put")) {
                app.put(route, handler.getHandler());
            }
        }
    }

    private Handler addRoute(String route, RouteHandler handler) {
        routeHandler.put(Route.getBaseRoute(domainName)+route, handler);
        return handler.getHandler();
    }

    public Handler addRoute(String route, Handler handler, String httpMethod) {
        RouteHandler newHandler = new RouteHandler();
        newHandler.setHandler(handler);
        newHandler.setHttpMethod(httpMethod);
        return addRoute(route, newHandler);
    }

    public Handler removeRoute(String baseDomainName, String route) {
       return routeHandler.remove(Route.getBaseRoute(baseDomainName)+route).getHandler();
    }

    private Handler updateRoute(String route, RouteHandler handler) {
        routeHandler.put(Route.getBaseRoute(domainName)+route, handler);
        return handler.getHandler();
    }

    public Handler updateRoute(String route, Handler handler) {
        RouteHandler currentHandler = routeHandler.get(Route.getBaseRoute(domainName)+route);
        currentHandler.setHandler(handler);
        return updateRoute(route, currentHandler);
    }

    public void printAllRoute() {
        logger.info("Printing all Routing....");
        List<String> allKey = new ArrayList<>(routeHandler.keySet());
        Collections.sort(allKey);
        for(String route: allKey) {
            logger.info(String.format("%s : %s ", routeHandler.get(route).getHttpMethod(), route));
        }
    }


}
