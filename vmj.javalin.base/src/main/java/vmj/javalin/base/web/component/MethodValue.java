package vmj.javalin.base.web.component;

import java.util.HashMap;

public class MethodValue<E> {
    private HashMap<String, E> valueHolders;

    public MethodValue(HashMap<String,E> valueHolders) {
        this.valueHolders = valueHolders;
    }

    public E getValue(String key) {
        return valueHolders.get(key);
    }


    public static class MethodValueBuilder<E> {

        private final HashMap<String, E> valueHolders = new HashMap<>();

        public MethodValueBuilder() {
        }

        public MethodValueBuilder<E> setMethodValue(String key, E value) {
            valueHolders.put(key, value);
            return this;
        }

        public MethodValue<E> build() {
            return new MethodValue<>(valueHolders);
        }

    }


}
