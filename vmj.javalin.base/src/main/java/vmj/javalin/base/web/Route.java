package vmj.javalin.base.web;

public class Route {

    private static final String BASE_URL = "/call";

    public static String getBaseRoute(String domainName) {
        return String.format("%s/%s/", BASE_URL, domainName);
    }

}
