package vmj.javalin.base.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vmj.javalin.base.utils.function.FunctionCall;

import java.util.HashMap;
import java.util.Map;

public abstract class Service {

    private Map<String, FunctionCall> serviceHandler = new HashMap<>();
    private Logger logger = LoggerFactory.getLogger(Service.class);



    public void addService(String methodName, FunctionCall methodHandler) {
        serviceHandler.put(methodName, methodHandler);
    }

    public <Any extends FunctionCall> Any getMethodByName(String methodName) throws Exception {
        Any method = null;
        try {
            method = (Any) serviceHandler.get(methodName);
        } catch (ClassCastException e) {
            logger.info("Failed to cast class" );
        }
        return method;
    }



}
