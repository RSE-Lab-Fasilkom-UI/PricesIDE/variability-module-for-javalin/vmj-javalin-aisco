package vmj.javalin.base.productline;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import vmj.javalin.base.factory.Entityfactory;
import vmj.javalin.base.productline.annotations.Core;
import vmj.javalin.base.productline.annotations.Delta;
import vmj.javalin.base.productline.annotations.Scope;
import vmj.javalin.base.productline.exception.FeatureNotExistException;
import vmj.javalin.base.productline.repository.FeatureRepository;
import vmj.javalin.base.productline.repository.ProductRepository;
import vmj.javalin.base.web.Controller;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.*;

@Getter
@Setter
@Slf4j
public abstract class ProductLine {

    private String productLineName;
    private List<Feature> features;

    public ProductLine(String name) {
        this.productLineName = name;
        configureProductLine();
    }

    public Product getProductByName(String name) {
        return ProductRepository.findByName(name);
    }

    public abstract void configureProductLine();

    public Product buildProduct(String productName) {
        log.trace("Creating new product " + productName);
        Product product = new Product(productName);
        List<String> featuresName = getProductFeatures();
        features = new ArrayList<>();
        for (String featureName : featuresName) {
            log.trace("Feature: ");
            log.trace(featureName);
            addFeature(featureName);
        }
        Collections.sort(features);

        product.setFeatures(features);

        log.trace("Component scan");

        // Component scan


        for (Feature feature : features) {
            log.trace("Tracing feature " + feature.getName());
            String type = getFeatureIdentifier(feature);
            log.trace(type);




            if (type.equalsIgnoreCase("core")) {
                log.trace("A core");
                CoreFeature coreFeature = (CoreFeature) feature;
                String basePackageName = String.format("%s.%s.%s", productLineName.toLowerCase(), coreFeature.getName().toLowerCase(), "core");
                String controllerPackage = basePackageName + ".controller";
                String modelPackage = basePackageName + ".model";
                String decoratorPackage = basePackageName+ ".decorator";

                Class<?> modelClass = null;
                Class<?>  controllerClass = null;
                Class<?> modelDecorator = null;

                try {
                    modelClass = getFirstClassFromPackage(modelPackage, Core.class);
                    controllerClass = getFirstClassFromPackage(controllerPackage, Core.class);
                    for (Class<?> decoratorClass : getClassFromPackage(decoratorPackage, Core.class)) {
                        Core classAnnotation = decoratorClass.getAnnotation(Core.class);
                        if(classAnnotation.scope()==Scope.MODEL) {
                            modelDecorator = decoratorClass;
                        }
                    }

                    coreFeature.setControllerName(controllerClass);
                    coreFeature.setModelName(modelClass);
                    coreFeature.setDecoratorModel(modelDecorator);
                    product.getDatabaseConfigurationBuilder().addClass(modelClass);
                    product.getDatabaseConfigurationBuilder().addClass(modelDecorator);
                    Controller controller = Entityfactory.createEntity(controllerClass);
                    feature.addControllerInstance(controllerClass.getTypeName(), controller);
                } catch (ClassNotFoundException e) {
                    log.error(e.getMessage());
                }

            } else {
                log.trace("Delta");
                DeltaFeature deltaFeature = (DeltaFeature) feature;
                String basePackageName = String.format("%s.%s.%s", productLineName.toLowerCase(), deltaFeature.getModifiedFeature().toLowerCase(), deltaFeature.getName().toLowerCase());
                try {
                    Set<Class<?>> annotatedClasses = getClassFromPackage(basePackageName, Delta.class);
                    for(Class<?> annotatedClass : annotatedClasses) {
                        Delta deltaDetail = annotatedClass.getAnnotation(Delta.class);
                        String baseFeature = deltaFeature.getModifiedFeature();
                        Scope classScope = deltaDetail.scope();
                        String dname = deltaDetail.name();

                        log.info(String.format("Processing: %s with scope %s", dname, classScope));

                        if(classScope== Scope.MODEL) {
                            product.getDatabaseConfigurationBuilder().addClass(annotatedClass);
                            deltaFeature.setModelName(annotatedClass);
                        } else {
                            Feature coreFeature = FeatureRepository.getFeatureByname(deltaFeature.getModifiedFeature());
                            Controller coreController = coreFeature.getController(coreFeature.getControllerName().getName());
                            Controller decoratedController  = Entityfactory.createEntity(annotatedClass, coreController);
                            deltaFeature.addControllerInstance(annotatedClass.getTypeName(), decoratedController);
                        }
                    }
                } catch (ClassNotFoundException e) {
                    log.error(e.getMessage());
                }

            }

        }

        product.build();

        return product;


    }

    private Class<?> getFirstClassFromPackage(String packageName,Class<? extends Annotation> targetAnnotation) throws ClassNotFoundException {

        Set<Class<?>> annotatedClasses = getClassFromPackage(packageName, targetAnnotation);
        Optional<Class<?>> firstAnnotatedClass = annotatedClasses.stream().findFirst();
        if (firstAnnotatedClass.isEmpty()) {
            throw new ClassNotFoundException(String.format("No annotated Class of type %s found", targetAnnotation.getName()));
        }
        return firstAnnotatedClass.get();
    }

    private Set<Class<?>> getClassFromPackage(String packageName, Class<? extends Annotation> targetAnnotation) throws ClassNotFoundException {
        Reflections reflections = new Reflections(packageName);
        log.trace(packageName);
        log.trace(targetAnnotation.getName());
        Set<Class<?>> annotatedClasses = reflections.getTypesAnnotatedWith(targetAnnotation);
        log.trace(String.valueOf(annotatedClasses.size()));
        if(annotatedClasses.isEmpty()) {
            throw new ClassNotFoundException(String.format("No annotated Class of type %s found", targetAnnotation.getName()));
        }
        return annotatedClasses;
    }


    private String getFeatureIdentifier(Feature feature) {
        if (feature.getDependenciesValue() < 0) {
            return "core";
        }
        return "delta";
    }


    private void addFeature(String featureName) {
        try {
            features.add(FeatureRepository.getFeatureByname(featureName));
        } catch (FeatureNotExistException e) {
            log.error(e.getMessage());
        }
    }


    private List<String> getProductFeatures() {
        log.info("Loading product resources");
        ObjectMapper mapper = new ObjectMapper();
        List<String> allFeatures = new ArrayList<>();
        try {
            log.trace("Opening Json");
            InputStream locationJson = this.getClass().getResourceAsStream("/static/json/features.json");

            log.trace("Converting json to array");
            allFeatures = Arrays.asList(mapper.readValue(locationJson, String[].class));
        } catch (Exception e) {
            log.error("Error: Please make sure /static/json/features.json exist");
            log.error(e.getMessage());
            log.error(e.getClass().getName());
        }

        return allFeatures;
    }


}


