package vmj.javalin.base.productline.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Delta {

    public String name() default "";
    public Scope scope() default Scope.MODEL;
    public String modifies() default "";
}
