package vmj.javalin.base.productline.configurator;

import vmj.javalin.base.productline.DeltaFeature;
import vmj.javalin.base.productline.repository.FeatureRepository;

import java.util.ArrayList;
import java.util.List;

public class DeltaConfigurator {
    private List<String> featureDependencies;
    private String name;
    private String featureName;
    private String modifiedFeature;

    public void init() {
        featureDependencies = new ArrayList<>();
    }


    public DeltaConfigurator define(String name) {
        init();
        this.name = name;
        return this;
    }

    public DeltaConfigurator when(String featureName) {
        this.featureName = featureName;
        return this;
    }

    public DeltaConfigurator after(String feature) {
        featureDependencies.add(feature);
        return this;
    }

    public DeltaConfigurator modifies(String modifiedFeature) {
        this.modifiedFeature = modifiedFeature;
        return this;
    }

    public void save() {
        DeltaFeature feature = new DeltaFeature();
        feature.setName(this.featureName);
        feature.setDeltaName(this.name);
        feature.setModifiedFeature(modifiedFeature);
        FeatureRepository.addFeature(feature);
    }

    public static void main(String[] args) {
        DeltaConfigurator deltaConfigurator = new DeltaConfigurator();

        deltaConfigurator.define("DIncome")
                .when("Income")
                .after("FinancialReport")
                .save();

        deltaConfigurator.define("DExpense")
                .when("Expense")
                .after("FinancialReport") //Delta
                .save();

    }


}
