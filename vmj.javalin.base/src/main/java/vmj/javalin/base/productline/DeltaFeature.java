package vmj.javalin.base.productline;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Getter @Setter
public class DeltaFeature extends Feature {

    private HashMap<String, Feature> featureMapping = new HashMap<>();
    private String deltaName;
    private String modifiedFeature;

    public boolean addDependencies(Feature feature) {
        if (featureMapping.get(feature.getName())==null) {
            featureMapping.put(feature.getName(), feature);
            return true;
        }
        return false;
    }

    public boolean removeDependencies(Feature feature) {
        if (featureMapping.get(feature.getName())!=null) {
            featureMapping.remove(feature.getName());
            return true;
        }
        return false;
    }

    public List<Feature> getDependencies() {
        return new ArrayList<>(featureMapping.values());
    }

    @Override
    public int getDependenciesValue() {
        return getDependencies().size();
    }
}
