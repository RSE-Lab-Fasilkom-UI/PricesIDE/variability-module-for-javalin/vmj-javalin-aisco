package vmj.javalin.base.productline;


import lombok.Getter;
import lombok.Setter;
import vmj.javalin.base.web.Controller;

import java.util.HashMap;


@Getter @Setter
public abstract class Feature implements Comparable<Feature> {

    private String name;

    private Class<?> modelName;

    private Class<?> controllerName;




    private HashMap<String, Controller> controllerInstance =  new HashMap<>();

    public abstract int getDependenciesValue();


    @Override
    public int compareTo(Feature o) {
        return getDependenciesValue()-o.getDependenciesValue();
    }

    public Controller getController(String controllerName) {
        return controllerInstance.get(controllerName);
    }

    public void addControllerInstance(String controllerName, Controller controller) {
        controllerInstance.put(controllerName, controller);
    }
}
