package vmj.javalin.base.productline;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CoreFeature extends Feature{

    private Class<?> decoratorModel;

    private Class<?> decoratorController;

    @Override
    public int getDependenciesValue() {
        return -1;
    }
}
