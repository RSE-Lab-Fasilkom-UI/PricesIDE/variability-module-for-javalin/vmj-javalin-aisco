package vmj.javalin.base.productline.repository;

import vmj.javalin.base.productline.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductRepository {

    private final static HashMap<String, Product> DATA_HOLDER = new HashMap<>();

    public static void addProduct(Product product) {
        DATA_HOLDER.put(product.getName(), product);
    }

    public static List<Product> findAll() {
        return new ArrayList<>(DATA_HOLDER.values());
    }

    public static Product findByName(String productName) {
        return DATA_HOLDER.get(productName);
    }


}
