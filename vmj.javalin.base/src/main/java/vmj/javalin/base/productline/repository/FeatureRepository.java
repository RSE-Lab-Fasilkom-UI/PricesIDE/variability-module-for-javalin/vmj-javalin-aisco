package vmj.javalin.base.productline.repository;

import vmj.javalin.base.productline.Feature;
import vmj.javalin.base.productline.exception.FeatureNotExistException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeatureRepository {

    private final static HashMap<String, Feature> DATA_HOLDER = new HashMap<>();

    public static Feature addFeature(Feature feature) {
        DATA_HOLDER.put(feature.getName(), feature);
        return feature;
    }

    public static Feature getFeatureByname(String featureName) throws FeatureNotExistException {
        Feature target = DATA_HOLDER.get(featureName);
        if(target!=null) {
            return target;
        }
        throw new FeatureNotExistException(featureName);
    }

    public static List<Feature> findAll() {
        return new ArrayList<>(DATA_HOLDER.values());
    }


}
