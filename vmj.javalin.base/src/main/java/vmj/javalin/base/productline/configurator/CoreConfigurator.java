package vmj.javalin.base.productline.configurator;

import vmj.javalin.base.productline.CoreFeature;
import vmj.javalin.base.productline.repository.FeatureRepository;

import java.util.ArrayList;

public class CoreConfigurator {
    private String name;
    private String featureName;



    public CoreConfigurator define(String name) {

        this.name = name;
        return this;
    }

    public CoreConfigurator when(String featureName) {
        this.featureName = featureName;
        return this;
    }


    public void save() {
        CoreFeature feature = new CoreFeature();
        feature.setName(this.featureName);
        FeatureRepository.addFeature(feature);
    }
}
