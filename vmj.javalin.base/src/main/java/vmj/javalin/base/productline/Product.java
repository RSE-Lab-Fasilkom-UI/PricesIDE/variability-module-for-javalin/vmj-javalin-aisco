package vmj.javalin.base.productline;

import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import lombok.Getter;
import lombok.Setter;
import vmj.javalin.base.data.DatabaseConfigurationBuilder;
import vmj.javalin.base.data.HibernateUtils;
import vmj.javalin.base.productline.repository.FeatureRepository;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class Product {
    private String name;
    private List<Feature> features;
    private DatabaseConfigurationBuilder databaseConfigurationBuilder;
    private Javalin app;

    public Product(String productName) {
        this.name =  productName;
        databaseConfigurationBuilder = new DatabaseConfigurationBuilder();
        app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).start(7776);


//        databaseConfigurationBuilder.addClass(FinancialReport.class);

       //HibernateUtils hibernateUtils = new HibernateUtils(databaseConfigurationBuilder);
    }

    public void build() {
        new HibernateUtils(databaseConfigurationBuilder);
        for(Feature feature: features) {
            if(feature.getDependenciesValue()<0) {
                feature.getController(feature.getControllerName().getName()).registerRoute(app);
            }
        }

    }


}
