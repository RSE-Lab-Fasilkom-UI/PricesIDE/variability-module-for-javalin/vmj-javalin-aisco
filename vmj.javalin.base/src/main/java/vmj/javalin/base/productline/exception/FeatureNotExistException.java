package vmj.javalin.base.productline.exception;

public class FeatureNotExistException extends RuntimeException {

    public FeatureNotExistException(String errorContent, String name) {
        super(String.format("%s %s", errorContent, name));
    }

    public FeatureNotExistException(String name) {
        this("Can't find features named: ", name);
    }
}
