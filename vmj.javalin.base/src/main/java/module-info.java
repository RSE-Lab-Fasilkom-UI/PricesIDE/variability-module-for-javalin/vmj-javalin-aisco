module vmj.javalin.base {
    requires io.javalin;
    requires static lombok;
    requires org.slf4j;
    requires org.hibernate.orm.core;
    requires java.naming;
    requires java.sql;
    requires net.bytebuddy;
    requires com.fasterxml.classmate;
    requires java.xml.bind;
    requires java.persistence;
    requires org.reflections;
    requires transitive com.fasterxml.jackson.databind;

    exports vmj.javalin.base.utils;
    exports vmj.javalin.base.web;
    exports vmj.javalin.base.web.component;
    exports vmj.javalin.base.handler;
    exports vmj.javalin.base.data;
    exports vmj.javalin.base.factory;
    exports vmj.javalin.base.utils.container;
    exports vmj.javalin.base.utils.function;
    exports vmj.javalin.base.productline;
    exports vmj.javalin.base.productline.annotations;
    exports vmj.javalin.base.productline.configurator;
    exports vmj.javalin.base.productline.exception;
    exports vmj.javalin.base.productline.repository;


}