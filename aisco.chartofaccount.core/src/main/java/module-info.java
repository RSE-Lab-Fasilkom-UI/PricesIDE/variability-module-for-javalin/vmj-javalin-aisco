module aisco.chartofaccount.core {
    requires io.javalin;
    requires static lombok;
    requires vmj.javalin.base;

    exports aisco.chartofaccount.core.controller;
    exports aisco.chartofaccount.core.model;
}