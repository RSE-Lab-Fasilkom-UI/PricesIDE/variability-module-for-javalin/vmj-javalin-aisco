package aisco.chartofaccount.core.model;

import lombok.Data;

@Data
public class ChartOfAccount {

    private int id;
    private String name;
    private String description;
    private String reference;
}
