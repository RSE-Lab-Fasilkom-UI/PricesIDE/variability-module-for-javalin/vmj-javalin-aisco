package aisco.chartofaccount.core.model;

import lombok.Data;

@Data
public class ChartOfAccountEntry {
    private int idChartOfAccount;
    private String name;
    private int amount;
    private String description;
    private int level;

}
