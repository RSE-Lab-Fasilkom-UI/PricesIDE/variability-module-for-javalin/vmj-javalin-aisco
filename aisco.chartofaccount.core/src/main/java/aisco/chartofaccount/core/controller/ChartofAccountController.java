package aisco.chartofaccount.core.controller;

import aisco.chartofaccount.core.model.ChartOfAccount;
import io.javalin.http.Handler;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.web.Controller;

import java.util.Map;

public class ChartofAccountController extends Controller {

    public ChartofAccountController() {
        setDomainName("chart-of-account");
        addRoute("create", createChartOfAccount, "POST");
        addRoute("list", findAllChartOfAccounts, "GET");
        addRoute("update", updateChartOfAccount, "PUT");
    }

    private Handler createChartOfAccount = ctx -> {
        ChartOfAccount chartOfAccount = ctx.bodyAsClass(ChartOfAccount.class);
        DatabaseUtilsInMemory.createData("chartofaccount", String.valueOf(chartOfAccount.getId()), chartOfAccount);
        ctx.json(chartOfAccount);
    };

    private Handler findAllChartOfAccounts =ctx -> {
        Map chartOfAccounts = DatabaseUtilsInMemory.getRecordsData("chart0faccount");
        ctx.json(chartOfAccounts.values());
    };

    private Handler updateChartOfAccount = ctx -> {
        ChartOfAccount updatedData = ctx.bodyAsClass(ChartOfAccount.class);
        DatabaseUtilsInMemory.updateData("chartofaccount", String.valueOf(updatedData.getId()), updatedData);
        ctx.json(updatedData);
    };
}
