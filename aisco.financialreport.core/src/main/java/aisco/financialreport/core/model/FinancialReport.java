package aisco.financialreport.core.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import vmj.javalin.base.productline.annotations.Core;
import vmj.javalin.base.productline.annotations.Scope;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "financialreport_impl")
@Inheritance(strategy = InheritanceType.JOINED)
@Core(name = "FinancialReport", scope = Scope.MODEL)
public class FinancialReport {


    @Id
    private int id;

    @Column
    private String datestamp;

    @Column
    private int amount;

    @Column
    private String description;

    @Column
    private String idProgram;

    @Column
    private String idCoa;



}
