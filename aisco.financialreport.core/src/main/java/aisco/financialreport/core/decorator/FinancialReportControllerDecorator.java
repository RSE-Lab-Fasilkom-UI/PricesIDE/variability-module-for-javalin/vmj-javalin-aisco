package aisco.financialreport.core.decorator;

import aisco.financialreport.core.controller.FinancialReportController;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vmj.javalin.base.productline.annotations.Core;
import vmj.javalin.base.productline.annotations.Scope;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Core(name = "FinancialReport", scope = Scope.CONTROLLER)
public  abstract  class FinancialReportControllerDecorator  extends FinancialReportController {

    private FinancialReportController financialReportController;
}
