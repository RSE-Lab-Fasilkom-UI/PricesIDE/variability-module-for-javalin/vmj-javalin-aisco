package aisco.financialreport.core.decorator;

import aisco.financialreport.core.model.FinancialReport;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vmj.javalin.base.productline.annotations.Core;
import vmj.javalin.base.productline.annotations.Scope;

import javax.persistence.CascadeType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@Getter
@Setter
@Core(name = "FinancialReport", scope = Scope.MODEL)
@MappedSuperclass
public abstract class FinancialReportDecorator extends FinancialReport {

    @OneToOne(cascade = CascadeType.ALL)
    private FinancialReport financialReport;


    public FinancialReportDecorator(FinancialReport financialReport) {
        this.financialReport = financialReport;
    }

    @Override
    public int getId() {
        return financialReport.getId();
    }

    @Override
    public String getDatestamp() {
        return financialReport.getDatestamp();
    }

    @Override
    public int getAmount() {
        return financialReport.getAmount();
    }

    @Override
    public String getDescription() {
        return financialReport.getDescription();
    }

    @Override
    public String getIdProgram() {
        return financialReport.getIdProgram();
    }

    @Override
    public String getIdCoa() {
        return financialReport.getIdCoa();
    }

    @Override
    public void setId(int id) {
        financialReport.setId(id);
    }

    @Override
    public void setDatestamp(String datestamp) {
        financialReport.setDatestamp(datestamp);
    }

    @Override
    public void setAmount(int amount) {
        financialReport.setAmount(amount);
    }

    @Override
    public void setDescription(String description) {
        financialReport.setDescription(description);
    }

    @Override
    public void setIdProgram(String idProgram) {
        financialReport.setIdProgram(idProgram);
    }

    @Override
    public void setIdCoa(String idCoa) {
        financialReport.setIdCoa(idCoa);
    }
}
