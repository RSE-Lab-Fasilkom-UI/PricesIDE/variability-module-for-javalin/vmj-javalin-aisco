package aisco.financialreport.core.controller;

import aisco.financialreport.core.model.FinancialReport;
import io.javalin.http.Handler;
import vmj.javalin.base.productline.annotations.Core;
import vmj.javalin.base.productline.annotations.Scope;
import vmj.javalin.base.utils.DatabaseUtilsInMemory;
import vmj.javalin.base.web.Controller;

import java.util.Map;


@Core(name = "FinancialReport", scope = Scope.CONTROLLER)
public class FinancialReportController extends Controller {

    public FinancialReportController() {
        setDomainName("financial-report");
        addRoute("create", createFinancialReport, "POST");
        addRoute("list", findAllFinancialReport, "GET");
        addRoute("update/{idFinancialReport}", updateFinancialReport, "PUT");

    }


    private Handler createFinancialReport = ctx -> {
        FinancialReport financialreport = ctx.bodyAsClass(FinancialReport.class);
        DatabaseUtilsInMemory.createData("financial-report", String.valueOf(financialreport.getId()),financialreport);
        ctx.json(financialreport);
    };

    private Handler findAllFinancialReport = ctx -> {
        Map allReports = DatabaseUtilsInMemory.getRecordsData("financial-report");
        ctx.json(allReports.values());
    };

    private Handler updateFinancialReport = ctx -> {
        String financialReportId = ctx.pathParam("idFinancialReport");
        FinancialReport financialReport = ctx.bodyAsClass(FinancialReport.class);
        DatabaseUtilsInMemory.updateData("financial-report", financialReportId,financialReport );
        ctx.json(financialReport);
    };
}
