module aisco.financialreport.core {
    requires io.javalin;
    requires static lombok;
    requires aisco.program.core;
    requires vmj.javalin.base;
    requires org.hibernate.orm.core;
    requires java.persistence;

    exports aisco.financialreport.core.controller;
    exports aisco.financialreport.core.decorator;
    exports aisco.financialreport.core.model;


    opens aisco.financialreport.core.model;
    opens aisco.financialreport.core.decorator;
    opens aisco.financialreport.core.controller;
}