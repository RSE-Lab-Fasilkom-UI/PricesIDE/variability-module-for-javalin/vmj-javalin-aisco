module product.custom.vmjtest {
    requires aisco.program.core;
    requires aisco.program.activity;
    requires aisco.program.operational;
    requires aisco.financialreport.core;
    requires aisco.financialreport.expense;
    requires aisco.financialreport.income;
    requires aisco.chartofaccount.core;
    requires aisco.automaticreport.core;
    requires aisco.automaticreport.twolevel;
    requires io.javalin;
    requires kotlin.stdlib;
    requires org.hibernate.orm.core;
    requires java.naming;
    requires java.sql;
    requires com.fasterxml.jackson.core;
    requires net.bytebuddy;
    requires com.fasterxml.classmate;
    requires java.xml.bind;
    requires vmj.javalin.base;
    requires productline.aisco;

    opens product.custom.vmjtest.app;
}