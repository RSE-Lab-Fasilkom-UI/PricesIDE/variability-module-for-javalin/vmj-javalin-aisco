module aisco.program.operational {
    requires aisco.program.core;
    requires io.javalin;
    exports aisco.program.operational;
}