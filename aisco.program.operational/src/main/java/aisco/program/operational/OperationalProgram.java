package aisco.program.operational;

import aisco.program.core.decorator.ProgramDecorator;
import aisco.program.core.model.Program;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationalProgram  extends ProgramDecorator {

    public OperationalProgram(Program program) {
        super(program);
    }

    private int idOperational;

    @Override
    public void setExecutionDate(String executionDate) {
        throw new UnsupportedOperationException();
    }
}
