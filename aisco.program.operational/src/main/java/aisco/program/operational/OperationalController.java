package aisco.program.operational;

import aisco.program.core.controller.ProgramController;
import aisco.program.core.decorator.ProgramControllerDecorator;
import io.javalin.http.Handler;

public class OperationalController extends ProgramControllerDecorator {

    public OperationalController(ProgramController programController) {
        super(programController);
        programController.setDomainName("operational");
        programController.addRoute("setIdOperational", setIdOperational, "POST");
        programController.removeRoute("program","setExecutionDate");
    }

    private Handler setIdOperational = ctx -> {
        int idProgram = Integer.parseInt(ctx.pathParam("idProgram"));
        int idOperational = Integer.parseInt(ctx.body());

        OperationalProgram opProgram = new OperationalProgram(ProgramController.getDatabase().get(idProgram));
        opProgram.setIdOperational(idOperational);

        ProgramController.getDatabase().put(idProgram, opProgram);
        ctx.json(opProgram);
    };
}
